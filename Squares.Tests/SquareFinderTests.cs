﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Squares.RestApi.Domain;
using System.Linq;

namespace Squares.Tests
{
    [TestClass]
    public class SquareFinderTests
    {
        [TestMethod]
        public void SuccessfullyFindsSquare()
        {
            // Arrange
            var plane = new Plane();
            plane.AddPoint(new Point(1, 1));
            plane.AddPoint(new Point(1, 2));
            plane.AddPoint(new Point(0, 1));
            plane.AddPoint(new Point(0, 2));
            plane.AddPoint(new Point(5, 7));
            plane.AddPoint(new Point(-1, 2));
            var squareFinder = new SquareFinder(plane);

            // Act
            var res = squareFinder.Execute();

            // Assert
            Assert.AreEqual(1, res.Count());
        }
    }
}