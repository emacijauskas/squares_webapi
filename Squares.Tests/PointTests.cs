﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Squares.RestApi.Domain;

namespace Squares.Tests
{
    [TestClass]
    public class PointTests
    {
        [TestMethod]
        public void PointFallsWithinBoundaries()
        {
            // Arrange
            var pt = new Point(1, 1);

            // Act
            var result = pt.FallsWithinBoundaries(0, 5);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void PointDoesNotFallWithinBoundaries()
        {
            // Arrange
            var pt = new Point(1, 10);

            // Act
            var result = pt.FallsWithinBoundaries(0, 6);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TwoPointsAreEqual()
        {
            // Arrange
            var pt1 = new Point(1, 5);
            var pt2 = new Point(1, 5);

            // Act
            var result = pt1.Equals(pt2);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TwoPointsAreNotEqual()
        {
            // Arrange
            var pt1 = new Point(1, 5);
            var pt2 = new Point(5, 1);

            // Act
            var result = pt1.Equals(pt2);

            // Assert
            Assert.IsFalse(result);
        }
    }
}