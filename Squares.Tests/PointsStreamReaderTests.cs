﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Squares.RestApi.Domain;
using System;
using System.IO;
using System.Linq;

namespace Squares.Tests
{
    [TestClass]
    public class PointsStreamReaderTests
    {
        [TestMethod]
        public void SuccessfullyReadsPointsFromStream()
        {
            // Arrange
            var s = "1 2" + Environment.NewLine + "5 6";
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            var pointsReader = new PointStreamReader(stream, 2);

            // Act
            var r = pointsReader.ReadPoints();

            // Assert
            Assert.AreEqual(r.Count(), 2);
        }

        [TestMethod]
        public void SuccessfullyReadsGivenAmountOfPointsFromStream()
        {
            // Arrange
            var s = "1 2" + Environment.NewLine + "5 6";
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            var pointsReader = new PointStreamReader(stream, 1);

            // Act
            var r = pointsReader.ReadPoints();

            // Assert
            Assert.AreEqual(r.Count(), 1);
        }
    }
}