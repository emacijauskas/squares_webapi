﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Squares.RestApi.Domain;
using System.Linq;
using System.Collections.Generic;

namespace Squares.Tests
{
    [TestClass]
    public class PlaneTests
    {
        [TestMethod]
        public void AddPointSuccessfully()
        {
            // Arrange
            var plane = new Plane();
            var point = new Point(1, 1);

            // Act
            var result = plane.AddPoint(point);

            // Assert
            Assert.IsTrue(result.HasSucceeded);
            Assert.AreEqual(plane.Points.Count(), 1);
            Assert.AreEqual(plane.Points.First(), point);
        }

        [TestMethod]
        public void DuplicatePointWontBeAdded()
        {
            // Arrange
            var plane = new Plane();
            var point = new Point(1, 1);
            plane.AddPoint(point);

            // Act
            var result = plane.AddPoint(point);

            // Assert
            Assert.IsFalse(result.HasSucceeded);
            Assert.AreEqual(plane.Points.Count(), 1);
        }

        [TestMethod]
        public void OutOfBoundPointWontBeAdded()
        {
            // Arrange
            var plane = new Plane();
            var point = new Point(10000, -10000);

            // Act
            var result = plane.AddPoint(point);

            // Assert
            Assert.IsFalse(result.HasSucceeded);
            Assert.AreEqual(plane.Points.Count(), 0);
        }

        [TestMethod]
        public void BulkAddIsSuccessful()
        {
            // Arrange
            var plane = new Plane();
            var points = new List<Point> { new Point(1, 1), new Point(1, 2), new Point(1, 3) };

            // Act
            var result = plane.BulkAddPoints(points);

            // Assert
            Assert.IsTrue(result.HasSucceeded);
            Assert.AreEqual(plane.Points.Count(), 3);
        }

        [TestMethod]
        public void BulkAddWillSkipDuplicates()
        {
            // Arrange
            var plane = new Plane();
            var points = new List<Point> { new Point(1, 1), new Point(1, 2), new Point(1, 3), new Point(1, 3), new Point(1, 5) };

            // Act
            var result = plane.BulkAddPoints(points);

            // Assert
            Assert.IsFalse(result.HasSucceeded);
            Assert.AreEqual(plane.Points.Count(), 4);
        }

        [TestMethod]
        public void BulkAddWillSkipIfBoundaryIsExceeded()
        {
            // Arrange
            var plane = new Plane();
            var points = new List<Point> { new Point(1, 1), new Point(-9000, 2), new Point(1, 3), new Point(1, 5) };

            // Act
            var result = plane.BulkAddPoints(points);

            // Assert
            Assert.IsFalse(result.HasSucceeded);
            Assert.AreEqual(plane.Points.Count(), 3);
        }

        [TestMethod]
        public void ClearsPlaneSuccessfully()
        {
            // Arrange
            var plane = new Plane();
            var points = new List<Point> { new Point(1, 1), new Point(1, 3), new Point(1, 5) };
            plane.BulkAddPoints(points);

            // Act
            plane.Clear();

            // Assert
            Assert.AreEqual(plane.Points.Count(), 0);
        }

        [TestMethod]
        public void RemovesPointSuccessfully()
        {
            // Arrange
            var plane = new Plane();
            var points = new List<Point> { new Point(1, 1), new Point(1, 3), new Point(1, 5) };
            plane.BulkAddPoints(points);

            // Act
            plane.RemovePoint(new Point(1, 1));

            // Assert
            Assert.AreEqual(plane.Points.Count(), 2);
        }
    }
}