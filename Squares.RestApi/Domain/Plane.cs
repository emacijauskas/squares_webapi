﻿using Squares.RestApi.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Squares.RestApi.Domain
{
    public class Plane
    {
        public const int UpperLimit = 5000;
        public const int LowerLimit = -5000;

        private List<Point> _points;

        public IEnumerable<Point> Points => _points.AsReadOnly();

        public int SpaceLeft => 10000 - _points.Count;

        public Plane()
        {
            _points = new List<Point>();
        }

        public BasicCommandResult AddPoint(Point pt)
        {
            var result = new BasicCommandResult();
            if (!pt.FallsWithinBoundaries(LowerLimit, UpperLimit))
            {
                result.AddError("Coordinate exceeds planes boundaries");
            }

            if (SpaceLeft == 0)
            {
                result.AddError("Plane has reached coordinates count limit");
            }

            var alreadyExists = _points.Any(point => point.X == pt.X && point.Y == pt.Y);
            if (alreadyExists)
            {
                result.AddError("Coordinate already exists");
            }

            if (result.HasSucceeded)
            {
                _points.Add(pt);
            }

            return result;
        }

        public BasicCommandResult BulkAddPoints(IEnumerable<Point> newPointsArr)
        {
            var result = new BasicCommandResult();
            if (newPointsArr == null)
            {
                throw new ArgumentNullException(nameof(newPointsArr));
            }

            var totalPointsToImport = newPointsArr.Count();
            if (SpaceLeft < totalPointsToImport)
            {
                totalPointsToImport = SpaceLeft;
                result.AddError($"Too many coordinates provided. Space left: {totalPointsToImport}");
            }

            var mergedPoints = newPointsArr
                .Take(totalPointsToImport)
                .Concat(_points)
                .ToList();

            var totalRemoved = mergedPoints
                .RemoveAll(pt => !pt.FallsWithinBoundaries(LowerLimit, UpperLimit));

            _points = mergedPoints
                .Distinct()
                .ToList();

            if (_points.Count < mergedPoints.Count + totalRemoved)
            {
                result.AddError("Duplicate coordinates skipped");
            }

            if (totalRemoved > 0)
            {
                result.AddError("Coordinates that exceeded plane boundaries were skipped");
            }

            return result;
        }

        public void Clear()
        {
            _points.Clear();
        }

        public BasicCommandResult RemovePoint(Point pt)
        {
            var result = new BasicCommandResult();

            var pointFound = _points
                .FirstOrDefault(p => p.Equals(pt));

            if (pointFound.X == pt.X && pointFound.Y == pt.Y)
            {
                _points.Remove(pointFound);
            }
            else
            {
                result.AddError("Point does not exist");
            }

            return result;
        }

        public IEnumerable<int[]> ToArray()
        {
            return _points.Select(point => point.ToArray());
        }
    }
}