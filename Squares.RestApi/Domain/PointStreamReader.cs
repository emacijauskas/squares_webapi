﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Squares.RestApi.Domain
{
    public class PointStreamReader
    {
        private readonly List<Point> _points;
        private readonly Stream _stream;
        private readonly int _amountOfPointsToRead;

        public PointStreamReader(Stream pointsStream, int amountOfPointsToRead)
        {
            if (pointsStream == null)
            {
                throw new ArgumentNullException(nameof(pointsStream));
            }

            if (amountOfPointsToRead == 0)
            {
                throw new ArgumentOutOfRangeException(nameof(amountOfPointsToRead));
            }

            _points = new List<Point>();
            _amountOfPointsToRead = amountOfPointsToRead;
            _stream = pointsStream;
        }

        public IEnumerable<Point> ReadPoints()
        {
            using (var reader = new StreamReader(_stream, Encoding.UTF8))
            {
                string line;
                while ((line = reader.ReadLine()) != null && _points.Count < _amountOfPointsToRead)
                {
                    int x, y;
                    var arr = line.Split(' ');
                    if (arr.Count() == 2 && int.TryParse(arr[0], out x) && int.TryParse(arr[1], out y))
                    {
                        _points.Add(new Point(x, y));
                    }
                }
            }

            return _points;
        }
    }
}