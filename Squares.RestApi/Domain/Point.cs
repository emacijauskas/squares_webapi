﻿namespace Squares.RestApi.Domain
{
    public struct Point
    {
        private int _x, _y;

        public int X => _x;
        public int Y => _y;

        public Point(int x, int y)
        {
            _x = x;
            _y = y;
        }

        public int[] ToArray()
        {
            return new[] { X, Y };
        }

        public bool FallsWithinBoundaries(int lowerLimit, int upperLimit)
        {
            return !(_x < lowerLimit || _x > upperLimit || _y < lowerLimit || _y > upperLimit);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Point))
            {
                return false;
            }

            var point = (Point)obj;
            return point.X == X && point.Y == Y;
        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash = (hash * 7) + X.GetHashCode();
            hash = (hash * 7) + Y.GetHashCode();
            return hash;
        }
    }
}