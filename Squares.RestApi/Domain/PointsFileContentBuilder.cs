﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Squares.RestApi.Domain
{
    public class PointsFileContentBuilder
    {
        private IEnumerable<Point> _points;

        public PointsFileContentBuilder(IEnumerable<Point> points)
        {
            if (points == null)
            {
                throw new ArgumentNullException(nameof(points));
            }

            _points = points;
        }

        public string ToFileContent()
        {
            var sb = new StringBuilder();

            foreach (var pt in _points)
            {
                sb.AppendFormat($"{pt.X} {pt.Y}{Environment.NewLine}");
            }

            return sb.ToString();
        }
    }
}