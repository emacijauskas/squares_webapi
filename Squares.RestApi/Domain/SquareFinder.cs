﻿using Squares.RestApi.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Squares.RestApi.Domain
{
    public class SquareFinder
    {
        private readonly Plane _plane;

        public SquareFinder(Plane plane)
        {
            if (plane == null)
            {
                throw new ArgumentNullException(nameof(plane));
            }

            _plane = plane;
        }

        public IEnumerable<IEnumerable<int[]>> Execute()
        {
            var sortedPoints = _plane.Points
                .OrderBy(p => p.X);

            var squares = new List<List<Point>>();
            var coll = new Dictionary<int, List<Point>>();
            var temp = new List<Point>();

            foreach (var point in sortedPoints)
            {
                List<Point> list;
                if (!coll.TryGetValue(point.X, out list))
                {
                    coll[point.X] = new List<Point> { point };
                }
                else
                {
                    if (list.Count > 0)
                    {
                        foreach (var pt in list)
                        {
                            var rotatedCorners = CalculateRotatedSquareMissingCorners(pt, point);
                            CheckIfPointsExist(sortedPoints, squares, point, pt, rotatedCorners);

                            var rightSideCorners = CalculateRightSideSquareMissingCorners(pt, point);
                            CheckIfPointsExist(sortedPoints, squares, point, pt, rightSideCorners);
                        }
                    }
                    list.Add(point);
                }
            }

            return squares.Select(square => square.Select(point => point.ToArray()));
        }

        private static void CheckIfPointsExist(IOrderedEnumerable<Point> sortedPoints, List<List<Point>> squares, Point point, Point pt, IEnumerable<Point> missingPoints)
        {
            var missingCornersFound = 0;

            foreach (var pointNeeded in missingPoints)
            {
                var doesExist = sortedPoints.Any(p => p.Equals(pointNeeded));
                if (!doesExist)
                {
                    break;
                }

                missingCornersFound++;
            }

            if (missingCornersFound == 2)
            {
                var missingPointsList = missingPoints.ToList();

                missingPointsList.Add(point);
                missingPointsList.Add(pt);
                missingPointsList.OrderBy(p => p.X).ThenBy(p => p.Y);

                squares.Add(missingPointsList);
            }
        }

        private IEnumerable<Point> CalculateRightSideSquareMissingCorners(Point pt1, Point pt2)
        {
            Point lowerCorner;
            Point upperCorner;

            if (pt1.Y > pt2.Y)
            {
                lowerCorner = pt2;
                upperCorner = pt1;
            }
            else
            {
                lowerCorner = pt1;
                upperCorner = pt2;
            }

            var distanceBetweenCorners = pt1.GetDistance(pt2);
            var calculatedCorners = new List<Point>();

            var lowerX = lowerCorner.X + distanceBetweenCorners;
            var upperX = upperCorner.X + distanceBetweenCorners;

            if (lowerX > Plane.LowerLimit && lowerX < Plane.UpperLimit && upperX > Plane.LowerLimit && upperX < Plane.UpperLimit)
            {
                calculatedCorners.Add(new Point(lowerX, lowerCorner.Y));
                calculatedCorners.Add(new Point(upperX, upperCorner.Y));
            }

            return calculatedCorners;
        }

        private IEnumerable<Point> CalculateRotatedSquareMissingCorners(Point pt1, Point pt2)
        {
            Point lowerCorner;
            Point upperCorner;

            var distanceBetweenCorners = pt1.GetDistance(pt2);
            var calculatedCorners = new List<Point>();

            if (distanceBetweenCorners % 2 != 0)
            {
                return calculatedCorners;
            }

            if (pt1.Y > pt2.Y)
            {
                lowerCorner = pt2;
                upperCorner = pt1;
            }
            else
            {
                lowerCorner = pt1;
                upperCorner = pt2;
            }

            var halfDiagonal = distanceBetweenCorners / 2;

            var rightX = lowerCorner.X + halfDiagonal;
            var leftX = lowerCorner.X - halfDiagonal;
            var axisY = lowerCorner.Y + halfDiagonal;

            if (rightX > Plane.LowerLimit && rightX < Plane.UpperLimit && leftX > Plane.LowerLimit && leftX < Plane.UpperLimit &&
                axisY > Plane.LowerLimit && axisY < Plane.UpperLimit)
            {
                calculatedCorners.Add(new Point(rightX, axisY));
                calculatedCorners.Add(new Point(leftX, axisY));
            }

            return calculatedCorners;
        }
    }
}