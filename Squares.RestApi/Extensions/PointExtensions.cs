﻿using Squares.RestApi.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squares.RestApi.Extensions
{
    public static class PointExtensions
    {
        public static int GetDistance(this Point pt1, Point pt2)
        {
            // Pythagorean theorem
            double distance = Math.Sqrt(Math.Pow(pt1.X - pt2.X, 2) + Math.Pow(pt1.Y - pt2.Y, 2));
            return Convert.ToInt32(distance);
        }
    }
}