﻿using Squares.RestApi.Common;
using Squares.RestApi.Domain;
using System.Web.Http;

namespace Squares.RestApi.Controllers
{
    public abstract class BaseController : ApiController
    {
        protected BaseController()
        {
        }

        protected IHttpActionResult OkResult(ICommandResult result)
        {
            var response = new
            {
                meta = new
                {
                    hasSucceeded = result.HasSucceeded,
                    errors = result.Errors
                }
            };

            return Ok(response);
        }
    }
}