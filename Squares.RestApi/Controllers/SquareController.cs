﻿using Squares.RestApi.Domain;
using Squares.RestApi.Models;
using System.Collections.Concurrent;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Squares.RestApi.Controllers
{
    public class SquareController : BaseController
    {
        private static Plane _defaultPlane;
        private static ConcurrentDictionary<string, Plane> _savedPlanes;

        public SquareController()
        {
            if (_savedPlanes == null)
            {
                _savedPlanes = new ConcurrentDictionary<string, Plane>();
            }

            if (_defaultPlane == null)
            {
                _defaultPlane = new Plane();
            }
        }

        [HttpPost]
        public IHttpActionResult AddPoint(PointViewModel model)
        {
            var plane = DecideSourcePlane(model.ListName);

            var pt = new Point(model.X, model.Y);
            var result = plane.AddPoint(pt);

            return OkResult(result);
        }

        [HttpPut]
        public IHttpActionResult ClearPlane(string listName)
        {
            var plane = DecideSourcePlane(listName);

            plane.Clear();

            return Ok();
        }

        [HttpDelete]
        public IHttpActionResult RemovePoint([FromBody]PointViewModel model)
        {
            var plane = DecideSourcePlane(model.ListName);

            var pt = new Point(model.X, model.Y);
            var result = plane.RemovePoint(pt);

            return OkResult(result);
        }

        [HttpGet]
        public IHttpActionResult GetPlane(int page, int perPage, string listName)
        {
            var plane = DecideSourcePlane(listName);

            var pagedPoints = plane
                .ToArray()
                .Skip(page * perPage)
                .Take(perPage);

            var result = new
            {
                points = pagedPoints,
                totalPoints = plane.Points.Count()
            };

            return Ok(result);
        }

        public IHttpActionResult GetSquares(int page, int perPage, string listName)
        {
            var plane = DecideSourcePlane(listName);

            var squares = new SquareFinder(plane)
                .Execute();

            var paged = squares
                .Skip(page * perPage)
                .Take(perPage);

            var result = new
            {
                squares = paged,
                totalSquares = squares.Count()
            };

            return Ok(result);
        }

        [HttpGet]
        public IHttpActionResult Download(string listName)
        {
            var plane = DecideSourcePlane(listName);

            var contentBuilder = new PointsFileContentBuilder(plane.Points);

            var result = new HttpResponseMessage(HttpStatusCode.OK);

            result.Content = new ByteArrayContent(Encoding.UTF8.GetBytes(contentBuilder.ToFileContent()));
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = "points.txt";

            var response = ResponseMessage(result);

            return response;
        }

        [HttpPost]
        public async Task<IHttpActionResult> Upload(string listName)
        {
            var plane = DecideSourcePlane(listName);

            var streamProvider = await Request.Content.ReadAsMultipartAsync(new MultipartMemoryStreamProvider());
            var stream = await streamProvider.Contents.First().ReadAsStreamAsync();

            var ptReader = new PointStreamReader(stream, plane.SpaceLeft);
            var result = plane.BulkAddPoints(ptReader.ReadPoints());

            return OkResult(result);
        }

        [HttpGet]
        public IHttpActionResult GetLists()
        {
            var lists = _savedPlanes
                .Select(p => p.Key)
                .OrderBy(p => p)
                .ToList();

            return Ok(lists);
        }

        [HttpPut]
        public IHttpActionResult SaveList(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return BadRequest();
            }

            if (_savedPlanes.ContainsKey(name))
            {
                _savedPlanes[name] = _defaultPlane;
            }

            _savedPlanes.TryAdd(name, _defaultPlane);
            _defaultPlane = new Plane();

            return Ok();
        }

        [HttpDelete]
        public IHttpActionResult DeleteList(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return BadRequest();
            }

            Plane plane;
            _savedPlanes.TryRemove(name, out plane);

            return Ok();
        }

        private Plane DecideSourcePlane(string name)
        {
            var plane = _defaultPlane;
            if (!string.IsNullOrEmpty(name) && _savedPlanes.ContainsKey(name))
            {
                _savedPlanes.TryGetValue(name, out plane);
            }

            return plane;
        }
    }
}