﻿using System.Collections.Generic;

namespace Squares.RestApi.Common
{
    public interface ICommandResult
    {
        IEnumerable<string> Errors { get; }
        bool HasSucceeded { get; }
    }
}