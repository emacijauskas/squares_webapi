﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Squares.RestApi.Common
{
    public class BasicCommandResult : ICommandResult
    {
        private List<string> _errors;

        public IEnumerable<string> Errors => _errors.AsReadOnly();

        public bool HasSucceeded => !_errors.Any();

        public BasicCommandResult()
        {
            _errors = new List<string>();
        }

        public void AddError(string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                throw new ArgumentNullException(nameof(message));
            }

            _errors.Add(message);
        }
    }
}