﻿namespace Squares.RestApi.Models
{
    public class PointViewModel
    {
        public int X { get; set; }
        public int Y { get; set; }
        public string ListName { get; set; }
    }
}